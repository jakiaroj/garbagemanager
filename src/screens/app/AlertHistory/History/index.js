import React, {Component} from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import {Card, CardItem, Right, Left, Body, Button} from 'native-base';
import styles from './styles';
import axios from '../../../../axios';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';

class History extends Component {
  constructor(props) {
    super(props);
    this.state = {
      biltiList: [],
      loading: false,
      skip: 0,
      scrolling: false,
      history: [],
    };
  }
  componentDidMount() {
    this.getHistory();
  }

  getHistory = async () => {
    await this.setState({
      loading: true,
    });
    try {
      const accessToken = await AsyncStorage.getItem('accessToken');
      let history = await axios.get('api/manager_history', {
        headers: {
          Authorization: 'Bearer ' + accessToken,
        },
      });
      this.setState({
        history: history.data.success,
      });
      console.log('history', history.data);
    } catch (e) {
      alert('Couldnt get history');
    }
    await this.setState({
      loading: false,
    });
  };

  render() {
    //console.log('scrolling', this.state.biltiList)

    return (
      <View style={styles.container}>
        {this.state.loading === false ? (
          <View>
            <FlatList
              data={this.state.history}
              renderItem={({item, index}) => (
                <Card>
                  <CardItem header bordered>
                    <Left>
                      <Text style={styles.headerText}>
                        {index + 1}.{' '}
                        {moment(item.created_at).format(
                          'MMMM Do YYYY, h:mm:ss a',
                        )}
                      </Text>
                    </Left>
                  </CardItem>
                  <CardItem bordered>
                    <Left>
                      <Text>Alert Status</Text>
                    </Left>
                    <Right>
                      <Text style={{fontWeight: 'bold'}}>
                        {item.status === 0
                          ? 'Approval Pending'
                          : item.status === 1
                          ? 'Driver Notified'
                          : 'Completed'}
                      </Text>
                    </Right>
                  </CardItem>
                  <CardItem bordered>
                    <Left>
                      <Text>Driver Assigned</Text>
                    </Left>
                    <Right>
                      <Text>
                        {item.driver_name ? item.driver_name : 'Not Assigned'}
                      </Text>
                    </Right>
                  </CardItem>
                  <CardItem bordered>
                    <Left>
                      <Text>Location</Text>
                    </Left>
                    <Right>
                      <Text>{item.location_name}</Text>
                    </Right>
                  </CardItem>
                  <CardItem bordered>
                    <Left>
                      <Text>Alerted By</Text>
                    </Left>
                    <Right>
                      <Text>{item.manager_name}</Text>
                    </Right>
                  </CardItem>
                  <CardItem bordered>
                    <Left>
                      <Text>Message</Text>
                    </Left>
                    <Right>
                      <Text>{item.message}</Text>
                    </Right>
                  </CardItem>
                  <CardItem bordered>
                    <Left>
                      <Text>Alert Type</Text>
                    </Left>
                    <Right>
                      <Text style={{color: item.type, fontWeight: 'bold'}}>
                        {item.type.toUpperCase()}
                      </Text>
                    </Right>
                  </CardItem>
                </Card>
              )}
            />
          </View>
        ) : (
          <View>
            <ActivityIndicator size="small" color="black" />
          </View>
        )}
      </View>
    );
  }
}
export default History;
