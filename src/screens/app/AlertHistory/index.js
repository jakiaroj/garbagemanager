import React, {Component} from 'react';
import History from './History';
import colors from '../../../config/colors';

class BiltiList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: this.props.navigation.state.params.type,
    };
  }

  static navigationOptions = ({navigation}) => {
    const {state} = navigation;
    if (state.params != undefined) {
      return {
        title: state.params.type,
        headerStyle: {
          backgroundColor: colors.primary,
          borderWidth: 0,
        },
        headerTitleStyle: {
          fontWeight: '200',
        },
        headerTintColor: '#fff',
      };
    }
  };

  render() {
    return <History />;
  }
}

export default BiltiList;
