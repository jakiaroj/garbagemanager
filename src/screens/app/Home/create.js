import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {Searchbar} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Dropdown} from 'react-native-material-dropdown';
import {Button} from 'native-base';
import DefaultButton from '../../../components/defaultButton';
import colors from '../../../config/colors';
import styles from './styles';

class BiltiCreate extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={{flexDirection: 'row'}}>
          <Searchbar placeholder="Search for Customer" style={{flex: 1}} />
          <View
            style={{
              backgroundColor: colors.lightGrey,
              paddingHorizontal: 1,
              marginHorizontal: 16,
            }}
          />
          <Button
            style={styles.newCustomerButton}
            onPress={_ => this.props.navigation.navigate("CreateCustomer")}>
            <Icon name="person-add" size={24} color="#fff" />
            <Text style={{color: '#fff'}}>New</Text>
          </Button>
        </View>

        <Dropdown
          label="Order Type"
          data={orderTypes}
          dropdownPosition={0}
          itemColor={orderTypes.length}
        />

        <Dropdown
          label="Agent"
          data={agents}
          dropdownPosition={0}
          itemColor={agents.length}
        />

        <View style={{height: 16}}/>

        <DefaultButton onPress={_ => alert('Create clicked')}>
          CREATE
        </DefaultButton>

        <View
          style={{flex: 1, justifyContent: 'flex-end', alignItems: 'center'}}>
          <Icon name="location-on" size={48} color={colors.lightGrey} />
          <Text style={{color: colors.lightGrey, marginBottom: 60}}>
            Location Name
          </Text>
        </View>
      </View>
    );
  }
}

const orderTypes = [{label: 'Order Type 1', value: '1'}];

const agents = [{label: 'Silk', value: 'Silk'}];

export default BiltiCreate;
