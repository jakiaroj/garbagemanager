import React, {PureComponent} from 'react';
import {View} from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import {Text} from 'native-base';
import colors from '../../../config/colors';
import AsyncStorage from '@react-native-community/async-storage';

class ScanPay extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
    };
  }

  async componentDidMount() {
    const userId = await AsyncStorage.getItem('userId');
    this.setState({
      userId: userId,
    });
  }
  static navigationOptions = {
    title: 'My QR Code',
    headerStyle: {
      backgroundColor: colors.primary,
      borderWidth: 0,
    },
    headerTitleStyle: {
      fontWeight: '200',
    },
    headerTintColor: '#fff',
  };
  render() {
    return (
      <View style={{flex: 1, alignItems: 'center'}}>
        <View style={{marginTop: 50, alignItems: 'center'}}>
          {this.state.userId !== '' ? (
            <QRCode
              value={this.state.userId}
              size={200}
              //color={colors.primary}
            />
          ) : null}
        </View>
        <View style={{marginTop: 30, alignItems: 'center'}}>
          <Text style={{fontWeight: 'bold', paddingTop: 16, fontSize: 20}}>
            Show your QR code to the driver to complete your alert status.
          </Text>
        </View>
      </View>
    );
  }
}

export default ScanPay;
