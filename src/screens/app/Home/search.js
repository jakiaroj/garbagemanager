import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
} from 'react-native';
import styles from './styles';
import {Card, CardItem, Left, Button, Right} from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';
import axios from '../../../axios';
import colors from '../../../config/colors';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      location: '',
      loading: false,
    };
  }
  async componentDidMount() {
    const name = await AsyncStorage.getItem('name');
    const location = await AsyncStorage.getItem('location');
    this.setState({
      name: name,
      location: location,
    });
  }

  alertPressed = color => {
    Alert.alert(
      'Are you Sure?',
      `Send ${color.toUpperCase()} alert ?`,
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: async () => {
            this.setState({
              loading: true,
            });
            try {
              const userId = await AsyncStorage.getItem('userId');
              const locationId = await AsyncStorage.getItem('locationId');
              const accessToken = await AsyncStorage.getItem('accessToken');
              let post = await axios.post(
                `api/create_collection_request`,
                {
                  manager_id: userId,
                  type: color,
                  message: 'This is a test',
                  location_id: locationId,
                },
                {
                  headers: {
                    Authorization: 'Bearer ' + accessToken,
                  },
                },
              );
              alert('Alert Successfully Sent');
              console.log('post', post.data);
            } catch (e) {
              alert('Ooops. Failed');
            }
            this.setState({
              loading: false,
            });
          },
        },
      ],
      {cancelable: true},
    );
  };
  render() {
    return (
      <View style={styles.container}>
        {this.state.loading ? (
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <ActivityIndicator animating={true} size="large" />
            <Text>Sending Alert...</Text>
          </View>
        ) : (
          <View>
            <Card style={styles.card}>
              <CardItem style={styles.card}>
                <Left>
                  <Text style={styles.text}>Welcome {this.state.name}</Text>
                </Left>
                <Right>
                  <TouchableOpacity
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    onPress={() =>
                      this.props.navigation.navigate('BiltiList', {
                        type: 'History',
                      })
                    }>
                    <Icon name="history" size={24} style={styles.text} />
                    <Text style={styles.text}>History</Text>
                  </TouchableOpacity>
                </Right>
              </CardItem>
            </Card>
            <View style={styles.logoContainer}>
              <Image
                source={require('../../../assets/logo.png')}
                style={{
                  width: 150,
                  height: 150,
                  marginTop: 20,
                  marginBottom: 10,
                }}
                resizeMode="contain"
              />
              <Text
                style={{
                  marginTop: 10,
                  marginBottom: 0,
                  fontSize: 20,
                  fontWeight: 'bold',
                }}>
                Garbage Manager
              </Text>
              <Text
                style={{
                  marginTop: 10,
                  marginBottom: 10,
                  fontSize: 15,
                  fontWeight: 'bold',
                }}>
                Location : {this.state.location}
              </Text>
            </View>

            <View style={styles.buttonContainer}>
              <Button
                block
                style={{marginBottom: 10, backgroundColor: 'red'}}
                onPress={() => this.alertPressed('red')}>
                <Text style={styles.text}>ALERT RED</Text>
              </Button>
              <Button
                block
                style={{marginBottom: 10, backgroundColor: 'orange'}}
                onPress={() => this.alertPressed('orange')}>
                <Text style={styles.text}>ALERT ORANGE</Text>
              </Button>
              <Button
                block
                style={{marginBottom: 50, backgroundColor: 'green'}}
                onPress={() => this.alertPressed('green')}>
                <Text style={styles.text}>ALERT GREEN</Text>
              </Button>

              <Button
                block
                style={{backgroundColor: colors.primary}}
                onPress={() => this.props.navigation.navigate('BiltiSearch')}>
                <Text style={styles.text}>QR CODE</Text>
              </Button>
            </View>
          </View>
        )}
      </View>
    );
  }
}

export default Home;
