import { createStackNavigator } from 'react-navigation-stack';
import BiltiSearchScreen from "./search";

const BiltiSelectionTabs = createStackNavigator({
  Search: {
    screen: BiltiSearchScreen,
    navigationOptions: ({ navigation }) => ({
      header: null
    }),
  }
});

export default BiltiSelectionTabs;