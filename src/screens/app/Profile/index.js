import React, {Component} from 'react';
import {View} from 'react-native';
import {TextField} from 'react-native-material-textfield';
import Button from '../../../components/defaultButton';
import colors from '../../../config/colors';
import styles from './styles';
import {logout} from '../../../../redux/actions/loginAction';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      location: '',
      email: '',
      loading: false,
    };
  }
  async componentDidMount() {
    let name = await AsyncStorage.getItem('name');
    let email = await AsyncStorage.getItem('email');
    let location = await AsyncStorage.getItem('location');

    this.setState({
      name: name,
      email: email,
      location: location,
    });
  }

  logout = async () => {
    await this.setState({
      loading: true,
    });
    await this.props.logout();
    await AsyncStorage.removeItem('accessToken');
    await AsyncStorage.removeItem('name');
    await AsyncStorage.removeItem('email');
    await AsyncStorage.removeItem('locationId');
    await AsyncStorage.removeItem('location');
    await AsyncStorage.removeItem('userId');

    await this.setState({
      loading: false,
    });
    this.props.navigation.navigate('Auth');
  };

  static navigationOptions = {
    title: 'Profile Details',
    headerStyle: {
      backgroundColor: colors.primary,
      borderWidth: 0,
    },
    headerTitleStyle: {
      fontWeight: '200',
    },
    headerTintColor: '#fff',
  };
  render() {
    const {name, location, email} = this.state;
    return (
      <View style={styles.container}>
        <TextField
          label="Manager Name"
          autoCapitalize="words"
          returnKeyType="next"
          value={name}
          disabled
        />
        <TextField
          label="Email"
          keyboardType="email-address"
          autoCapitalize="none"
          returnKeyType="next"
          value={email}
          disabled
        />

        <TextField
          label="Location Assigned"
          value={location}
          onChangeText={value => this.setState({location: value})}
          disabled
        />

        <View style={{height: 16}} />

        <Button
          warning
          onPress={this.logout}
          loading={this.state.loading}
          disabled={this.state.loading}>
          Logout
        </Button>
      </View>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(logout()),
  };
};
const mapStateToProps = state => {
  return {
    Auth: state.Auth,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Profile);
