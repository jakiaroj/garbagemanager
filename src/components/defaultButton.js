import React from 'react';
import {Button} from 'react-native-paper';

export default DefaultButton = props => {
  return (
    <Button
      mode="contained"
      contentStyle={{height: 56}}
      style={{marginBottom: 16}}
      uppercase={false}
      {...props}
    />
  );
};
