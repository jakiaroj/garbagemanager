import React, {Component} from 'react';
import { TouchableOpacity } from "react-native";
import Menu from 'react-native-material-menu';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class CustomMenu extends Component {
  _menu = null;

  setMenuRef = ref => {
    this._menu = ref;
  };

  hideMenu = () => {
    this._menu.hide();
  };

  showMenu = () => {
    this._menu.show();
  };

  render() {
    return (
      <Menu
        ref={ref => (this._menu = ref)}
        button={
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Profile")}
            style={{
              paddingHorizontal: 16,
              height: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Icon name="account" size={24} color="#fff"/>
          </TouchableOpacity>
        }>
      </Menu>
    );
  }
}
