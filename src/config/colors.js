export default {
  primary: '#346BB2',
  //primaryLight: '#ffefe5',
  grey: '#808080',
  darkGrey: '#636e72',
  lightGrey: '#D3D3D3',
  inactive: '#39393a',
  danger: '#FF0000',
  success: '#279745',
  white: '#fff',
  disabledText: '#858585',
  disabledButton: '#f7f7f7',
};
