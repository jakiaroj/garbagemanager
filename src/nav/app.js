import React from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import Menu from '../components/menu';
import colors from '../config/colors';
import Home from '../screens/app/Home';
import Profile from '../screens/app/Profile';
import BiltiList from '../screens/app/AlertHistory';
import BiltiSearch from '../screens/app/Home/biltiSearch';

const AppStack = createStackNavigator({
  Home: {
    screen: Home,
    navigationOptions: ({navigation}) => ({
      title: 'Home',
      headerTintColor: '#fff',
      headerStyle: {
        backgroundColor: colors.primary,
      },
      headerRight: <Menu navigation={navigation} />,
    }),
  },
  Profile: {
    screen: Profile,
  },
  BiltiList: {
    screen: BiltiList,
  },
  BiltiSearch: {
    screen: BiltiSearch,
  },
});

export default AppStack;
