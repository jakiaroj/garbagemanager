import React from 'react';
import { createAppContainer } from 'react-navigation';
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
import { Transition } from 'react-native-reanimated';
import SplashScreen from '../screens/splash';
import AuthNavigator from './auth';
import AppNavigator from "./app";



export default createAppContainer(
  createAnimatedSwitchNavigator(
    {
      Splash: SplashScreen,
      Auth: AuthNavigator,
      App: AppNavigator
    },
    {
      initialRouteName: 'Splash',
      transition: (
        <Transition.Together>
          <Transition.Out
            type="slide-bottom"
            durationMs={400}
            interpolation="easeIn"
          />
          <Transition.In type="fade" durationMs={500} />
        </Transition.Together>
      ),
    },
  ),
);
