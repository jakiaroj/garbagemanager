import axios from "axios";
export const url = "http://garbagetracker.tk/";
export default axiosInstance = axios.create({
   baseURL: url
});