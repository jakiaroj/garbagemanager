export const logInSuccess = (data) => {
    return (dispatch, getState) => {
        dispatch({ type: 'LOGIN_SUCCESS', data })
    }
}
export const logInError = (err) => {
    return (dispatch, getState) => {
        dispatch({ type: 'LOGIN_FAIL', err })
    }
}
export const logout = () => {
    return (dispatch, getState) => {
        dispatch({ type: 'LOGOUT' })
    }
}
