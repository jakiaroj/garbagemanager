import Auth from './authReducer';
import Bilti from './biltiReducer';
import { combineReducers } from 'redux';
const rootReducer = combineReducers({
    Auth,
    Bilti
})
export default rootReducer; 