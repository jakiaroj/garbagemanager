const initState = {
    accessToken: null,
    userId: null,
    loginError: null,
}

const authReducer = (state = initState, action) => {
    switch (action.type) {
        case "LOGIN_SUCCESS":
            return {
                ...state,
                accessToken: action.data.id,
                userId: action.data.userId,
                loginError: null
            }
        case "LOGIN_FAIL":
            return {
                ...state,
                loginError: 'error',
            }
        case "LOGOUT":
            return {
                ...state,
                accessToken: null,
                userId: null,
                userData: {},
                loginError: null
            }
        default:
            return state
    }
}

export default authReducer;