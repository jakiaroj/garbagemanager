const initState = {
    singleBilti: {},
    singleLoad: {}
}

const authReducer = (state = initState, action) => {
    switch (action.type) {
        case "SINGLE_BILTI":
            return {
                ...state,
                singleBilti: action.singleBilti
            }
        case "SINGLE_LOAD":
            return {
                ...state,
                singleLoad: action.singleLoad
            }
        default:
            return state
    }
}

export default authReducer;